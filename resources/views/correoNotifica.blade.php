<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Notificacion</title>
</head>
<body>
    <h4>
        Se ha realizado una venta a {!! $nombreCliente !!}        
    </h4>
    
    <br>

    <h3> Datos del Cliente</h3>
    <p> <b> Numero de identificacion: </b> {!! $identificacion !!} </p>
    <p> <b> Celular: </b> {!! $celular !!} </p>
    <p> <b> Correo del cliente: </b> {!! $correoCliente !!} </p>
    <p> <b> Direccion: </b> {!! $direccion !!} </p>
    <p> <b> Direccion Entrega: </b> {!! $direccionEntrega !!} </p>
    <p> <b> Numero contacto: </b> {!! $numeroContacto !!} </p>

    <br>

    <h3> Informacion del Plan Adquirido</h3>
    <p> <b> Codigo: </b> {!! $codigoPlan !!} </p>
    <p> <b> Plan: </b> {!! $nombrePlan !!} </p>    
    <p> <b> Tarifa Basica: </b> {!! $tarifaBasica !!} </p>
    <p> <b> Tarifa Final: </b> {!! $tarifaFinal !!} </p>
    <p> <b> Observacion: </b> {!! $Observacion !!} </p>
    
    <br>

    <h3>Forma de pago</h3>
    <p> <b> Tipo de Pago: </b> {!! $tipoPago !!} </p>
    <p> <b> Banco: </b> {!! $nombreBanco !!} </p>
    <p> <b> Numero de cuenta: </b> {!! $numeroCuenta !!} </p>
    
</body>
</html>