<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Usuario extends Model
{
    // Tabla
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Usuario';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoUsuario';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoEmpresa',
        'vchNombreUsuario',
        'vchApellidoUsuario',
        'vchDireccionUsuario',
        'vchConvencionalUsuario',
        'vchCelularUsuario',
        'vchCorreoUsuario',
        'vchClaveUsuario',
        'vchImagenUsuario',
        'intDistribuyeVenta',        
        'vchCargoAsignado',
        'intCodigoEstadoUsuario',
    ];

    public static function loginUsuario($data)
    {
        return Usuario::where('vchClaveUsuario', $data->claveUsuario)
                      ->where('vchCorreoUsuario', $data->emailUsuario)
                      ->where('intCodigoEstadoUsuario', 1)
                      ->first([
                          'intCodigoEmpresa as idEmpresa',
                          'intCodigoUsuario as idUsuario',
                          'vchNombreUsuario as nombre', 
                          'vchApellidoUsuario as apellido',
                          'vchImagenUsuario as imagen',
                          'vchCargoAsignado as cargo',
                          'intDistribuyeVenta as distribuyeVenta'
                      ]);                          
    }

    // Estadistica de llamadas por cliente
    public static function getEstadisticaLlamadas($data)
    {
        return null;
    }

    // Obtener Menus
    public static function obtenerMenu($data)
    {
        return DB::table('vw_MenuUsuario')
            ->where('intCodigoEmpresa', $data->idEmpresa)
            ->where('intCodigoUsuario', $data->idUsuario)
            ->orderBy('intOrdenMenu')
            ->get([
                'vchNombreMenu as nombre'
              , 'vchIconoMenu as iconoMenu'
              , 'vchRutaMenu as ruta'
              , 'intContieneSubMenu as contieneSubMenu'              
                 ]);            
    }

}
 