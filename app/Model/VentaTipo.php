<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VentaTipo extends Model
{
        /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'VentaTipo';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoVentaTipo';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNombreVentaTipo',
        'intCodigoEstado',
    ];

    public static function getVentaTipo()
    {
        return VentaTipo::where('intCodigoEstado', 1)
            ->get([
                'intCodigoVentaTipo as id',
                'vchNombreVentaTipo as nombre'
            ]);
    }    
}
