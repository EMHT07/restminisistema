<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Respuesta';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoRespuesta';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNombreRespuesta',
        'vchTipoRespuesta',
        'intCodigoEstadoRespuesta'
    ];

    public static function getRespuesta()
    {        
        return Respuesta::where('intCodigoEstadoRespuesta', 1)                     
            ->get([
                'intCodigoRespuesta as id',
                'vchTipoRespuesta as tipo',
                'vchNombreRespuesta as nombre'
            ]);
    }
}
