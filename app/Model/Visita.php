<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Visita';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoVisita';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoEst',
        'intVendeRecargas',
        'intInteresado',
        'dtmProximaVisita',
        'vchObservacionVisita',
        'decLatitud',
        'decLongitud',
        'intEstadoVisita',        
    ];
}