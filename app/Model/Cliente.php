<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Cliente extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Cliente';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoCliente';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [    
        'intCodigoSector',  
        'intCodigoNacionalidad',  
        'intCodigoEstadoCivil',
        'intCodigoIdentificacion',
        'vchIdentificacion',
        'vchApellidoPaterno',
        'vchApellidoMaterno',
        'vchNombreCliente',
        'vchConvencionalCliente',
        'vchCelularCliente',
        'vchCorreoCliente',
        'vchDireccionCliente',
        'vchNivelEducacion',
        'dtmFechaNacimiento',
        'dtmFechaFallecimiento',
        'dtmFechaRegistroCliente',
        'decValorConsumoAproximado',
        'intCodigoEstadoCliente',          
    ];

    public static function getDatosDetalleVenta($data)
    {
        $datos = DB::selectOne('spT_Venta_ReporteVenta ?, ?', [$data->idCliente, $data->idUsuario]);
        return $datos;
    }
  
}
