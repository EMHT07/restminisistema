<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Identificacion extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Identificacion';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoIdentificacion';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNombreIdentificacion',
        'intCodigoEstadoIdentificacion'
    ];

    // Obtener identificaciones
    public static function getIdentificacion()
    {
        return Identificacion::where('intCodigoEstadoIdentificacion', 1)
                                ->get([
                                    'intCodigoIdentificacion as id',
                                    'vchNombreIdentificacion as nombre'
                                ]);
    }
}

