<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'PlanCelular';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoPlanCelular';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNombreplan',
        'dblTarifaBasicaPlan',
        'dblTarifaFinalPlan',
        'dblTotalGigasPlan',
        'vchObservacionPlan',
        'intCodigoEstadoPlanCelular',
    ];

    public static function getPlanes()
    {
        return Plan::where('intCodigoEstadoPlanCelular', 1)
                ->orderBy('vchNombreplan', 'asc')
                ->get([
                    'intCodigoPlanCelular as id',
                    'vchNombreplan as nombre',
                ]);
    }
}
