<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Empresa';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoEmpresa';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNombreEmpresa',
        'vchDireccionEmpresa',
        'vchTelefonoEmpresa',
        'vchLogoEmpresa',
        'vchCorreoEmpresa',
        'intCodigoEstadoEmpresa'        
    ];


    // obtiene informacion de las empresas
    public static function informacion($idEmpresa)
    {
        return Empresa::where('intCodigoEmpresa', $idEmpresa)
                ->where('intCodigoEstadoEmpresa', 1)
                ->first([
                    'intCodigoEmpresa as id',
                    'vchNombreEmpresa as nombre',
                    'vchLogoEmpresa as logo',
                    'vchCorreoEmpresa as correo'
                    ]);
    }


    public static function getCorreosEmpresa($id)
    {
        return Empresa::where('intCodigoEmpresa', $id)
            ->first([
                'vchCorreoEmpresa as correo'
            ]);
    }



}
