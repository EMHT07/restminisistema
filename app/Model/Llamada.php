<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Llamada extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Llamada';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigollamada';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoRespuesta',
        'intCodigoUsuario',
        'intCodigoCliente',
        'dtmFechaAsignacionllamada',
        'dtmDuracionTiempollamada',
        'intCodigoEstadollamada',        
        'vchObservacionllamada',
        'intPrioridadllamada'
    ];

    // Obtener el listado de los clientes
    public static function getClientes($data)
    {
        return DB::select('spT_Cliente_AsignacionCallCenter ?', [$data->idCliente]);            
    }

    // Guardamos el registro de los clientes
    public static function setRegistroLlamada($data)
    {        

        // Registramos las ventas
        $responde = DB::selectOne('spC_LLamada_Venta_VentaFormaPago_Registro ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?', [
            $data->idUsuario,
            $data->idCliente,
            $data->cronometro,
            $data->contesto,
            $data->respuesta,
            $data->razon,
            $data->observacion,
        
            $data->planCelular,
            $data->tipoVenta,
            $data->telefonoPlan,
            $data->direccionEntrega,
            $data->numeroContacto,
            $data->formaPago,
            $data->indicadorDatoscompleto,
        
            $data->banco,
            $data->tipoCuenta,
            $data->numeroCuenta,
            $data->codigoSeguridad,
            $data->fechaCaducidad,            
            $data->fechaCita,                
            $data->horaCita,
        ]);        

        return $responde->respuesta;
    }

    // Mostrar estadistica de los clientes procesados
    public static function getEstadistica($data)
    {
        $datos = DB::selectOne('spT_Cliente_MuestraEstadistica ?', [$data->idUsuario]);        
        return $datos->respuesta;
    }   
    
    // Modificar la prioridad
    public static function setLlamarCliente($data) {
        Llamada::where('intCodigoCliente', $data->idCliente)
                ->update([
                    'intPrioridadllamada' => 1,
                    'intCodigoEstadollamada' => 1
                    ]);
    }
}
