<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Util extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Util';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoUtil';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchCodigoUtil',
        'vchTipoUtil',
        'vchNombreUtil',
        'vchModuloUtil',
        'intCodigoEstadoUtil',
    ];

    // Obtiene informacion dependiendo del modulo consultado
    public static function getDatos($data)
    {
        return Util::where('vchModuloUtil', $data)
            ->orderBy('intCodigoUtil', 'asc')
            ->get([
                'vchCodigoUtil as codigo',
                'vchTipoUtil as tipo',
                'vchNombreUtil as nombre',
            ]);
    }
}
