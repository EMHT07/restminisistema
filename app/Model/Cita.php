<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Cita extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Cita';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoCita';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoLlamada',
        'dtmFechaCita',
        'dtmHoraCita',
        'intEstadoCita',
        'intCodigoEstadoCita'        
    ];

    // Citas de los clientes
    public static function getCitas($data)
    {
        $fechaActual = date('Y-m-d');
        return DB::table('vw_ClienteCita')
            ->where('intCodigoEstadoCliente', 1)
            ->where('intCodigoEstadollamada', 2)
            ->where('intCodigoUsuario', $data->idUsuario)
            ->where('dtmFechaCita', $fechaActual)
            ->get([
                'intCodigoCliente as idCliente',
                'Nombre as nombre',
                'dtmFechaCita as fecha',
                'dtmHoraCita as hora',
            ]);
    }
}
