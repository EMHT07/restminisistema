<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Parroquia extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Parroquia';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoParroquia';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoCiudad',
        'vchNombreParroquia',        
        'intCodigoEstadoParroquia',        
    ];
}

