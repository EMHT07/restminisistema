<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LocalTipo extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'LocalTipo';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoLocalTipo';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNombreLocalTipo',
        'intCodigoEstadoLocalTipo'
    ];
}
