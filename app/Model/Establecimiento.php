<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Establecimiento extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Establecimiento';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoEst';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoSector',
        'intCodigoTipoEst',
        'vchRotuloEst',
        'vchCallePrincipalEst',
        'vchNumeroEst',
        'vchCalleSecundariaEst',
        'vchReferenciaEst',
        'vchLatitudEst',
        'vchLongitudEst',
        'vchConvencionalEst',
        'vchCelularClaro',
        'vchIconoPlataforma',
        'dtmFechaCreacionEst',
        'dtmFechaActualizacionEst',
        'intEstadoEst',        
    ];
}
