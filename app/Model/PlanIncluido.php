<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PlanIncluido extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'PlanIncluido';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoPlanIncluido';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNombrePlanIncluido',
        'intCantidadPlanIncluido',
        'intCodigoEstadoPlanIncluido',
    ];
}
