<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Nacionalidad extends Model
{
   
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Nacionalidad';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoNacionalidad';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNacionalidad',
        'intCodigoEstadoNacionalidad'
    ];
}
