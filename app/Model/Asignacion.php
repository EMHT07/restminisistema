<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Asignacion';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoAsignacion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoUsuario',
        'vchNombreAsignacion',
        'dtmFechaCreacion',
        'dtmFechaModificacion',
        'intEstadoAsignacion'        
    ];
}
