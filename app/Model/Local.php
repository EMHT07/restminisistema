<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'LOCAL';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoLocal';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoSector',
        'intCodigoLocalTipo',
        'vchRotuloLocal',
        'vchCallePrincipal',
        'vchNumeroLocal',
        'vchCalleSecundaria',
        'vchReferenciaLocal',
        'decLatitudLocal',
        'decLongitudLocal',
        'vchConvencionalLocal',
        'vchCelularClaro',
        'vchIconoPlataforma',
        'dtmFechaRegistroLocal',
        'dtmFechaActualizacionLocal',
        'intCodigoEstadoLocal',        
    ];
}
