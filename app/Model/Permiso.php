<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Permiso';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoPermiso';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoUsuario',
        'vchNombrePermiso',
        'dtmFechaCreacionPermiso',
        'dtmFechaModificacionPermiso',
        'intCodigoEstadoPermiso'        
    ];
}
