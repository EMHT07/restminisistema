<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FormaPago extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'FormaPago';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoFormaPago';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNombreFormaPago',
        'intCodigoEstado'
    ];

    public static function getFormaPago()
    {
        return FormaPago::where('intCodigoEstado', 1)
            ->get([
                'intCodigoFormaPago as id',
                'vchNombreFormaPago as nombre'
            ]);
    }    
}
