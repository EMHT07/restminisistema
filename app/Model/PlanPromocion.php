<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PlanPromocion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'PlanPromocion';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoPlanPromocion';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNombrePlanPromocion',
        'dblCantidadPlanPromocion',
        'intTiempoMesPlanPromocion',
        'intCodigoEstadoPlanPromocion',
    ];
}
