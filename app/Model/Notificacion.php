<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Notificacion';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoNotificacion';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoEmpresa',
        'vchCorreoNotificacion',
        'intEstadoNotificacion',
    ];

    // obtiene la lista de correos 
    public static function getCorreosNotifica($data)
    {
        return Notificacion::where('intCodigoEmpresa', $data->idEmpresa)
                           ->where('intEstadoNotificacion', 1)
                           ->get([
                               'vchCorreoNotificacion as para'
                           ]);
    }    
}
