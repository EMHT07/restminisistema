<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Suceso extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Suceso';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoSuceso';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoUsuario',
        'vchNombreSuceso',
        'vchObservaSuceso',
        'vchNombreMetodoSuceso',
        'dtmFechaRegistroSuceso'
    ];
}
