<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RazonDesinteres extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'RazonDesinteres';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoRazonDesinteres';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNombreRazonDesinteres',
        'vchDescripcionRazonDesinteres',
        'intCodigoEstado',
    ];

    public static function getRazon()
    {
        return RazonDesinteres::where('intCodigoEstado', 1)
            ->get([
                'intCodigoRazonDesinteres as id',
                'vchNombreRazonDesinteres as nombre'
            ]);
    }
}
