<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Banco';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoBanco';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchNombreBanco',
        'intCodigoEstado'
    ];

    public static function getBanco()
    {
        return Banco::where('intCodigoEstado', 1)
            ->get([
                'intCodigoBanco as id',
                'vchNombreBanco as nombre'
            ]);
    }
}
