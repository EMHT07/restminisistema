<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PlanTipo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'PlanTipo';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoPlanTipo';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchCodigoPlanTipo',
        'vchNombrePlanTipo',
        'intCodigoEstadoPlanTipo',
    ];
}
