<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class EstadoCivil extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Estadocivil';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoEstadoCivil';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vchEstadoCivil',
        'intCodigoEstadoEstadoCivil'
    ];
}
