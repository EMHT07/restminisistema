<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Ciudad';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'intCodigoCiudad';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intCodigoProvincia',    
        'vchNombreCiudad',    
        'intCodigoEstadoCiudad'
    ];
}
