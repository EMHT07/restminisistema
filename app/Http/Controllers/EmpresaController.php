<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Empresa;

class EmpresaController extends Controller
{
    //
    public function informacion(Request $request)
    {
        $data = Empresa::informacion($request->idEmpresa);

        return response()->json($data, 200);
    }
}
