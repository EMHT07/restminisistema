<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ParametroItem;

class ParametroController extends Controller
{
    // Obtiene valor del cronometro
    public function cronometro()
    {
        $data = ParametroItem::getCronometro();
        return response()->json($data, 200);
    }

    // Obtien las posibles respuestas
    public function respuestas()
    {
        $data = ParametroItem::getRespuestas();
        return response()->json($data, 200);
    }
}
