<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Util;
use App\Model\Identificacion;
use App\Model\RazonDesinteres;
use App\Model\VentaTipo;
use App\Model\Banco;
use App\Model\FormaPago;
use App\Model\Respuesta;

class UtilController extends Controller
{
    // Datos del Maestro
    public function getDatos(Request $request)
    {
        $data = Util::getDatos($request->modulo);
        return response()->json($data, 200);       
    }

    // Datos del Identificacion
    public function getIdentificacion()
    {
        $data = Identificacion::getIdentificacion();
        return response()->json($data, 200);
    }

    public function razon()
    {
        $data = RazonDesinteres::getRazon();
        return response()->json($data, 200);
    }

    public function ventaTipo()
    {
        $data = VentaTipo::getVentaTipo();
        return response()->json($data, 200);
    }   
    
    public function banco()
    {
        $data = Banco::getBanco();
        return response()->json($data, 200);
    } 
    
    public function formaPago()
    {
        $data = FormaPago::getFormaPago();
        return response()->json($data, 200);
    }   
    
    public function respuesta()
    {
        $data = Respuesta::getRespuesta();
        return response()->json($data, 200);
    }       

}
