<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Cliente;
use App\Model\Empresa;
use Mail;

class NotificacionController extends Controller
{
    private $correoEmpresa;

    public function envioCorreo(Request $request){                     

        $data = Cliente::getDatosDetalleVenta($request); 

        if (isset($data)) {            

            $dataArray = json_decode(json_encode($data), true);

            $dataEmpresa = Empresa::getCorreosEmpresa($request->idEmpresa);                    
            
            $this->correoEmpresa = $dataEmpresa->correo;        
    
            Mail::send('correoNotifica', $dataArray, function ($message) {
                $message->to($this->correoEmpresa);
                $message->subject('Venta concretada');            
            });

            $respuest = array('mensaje' => 'Se ha enviado una notificación a activaciones '.$this->correoEmpresa );
            
            $data = ['mensaje' => 'Se envio correo a '.$this->correoEmpresa];
            return response()->json($data, 200);
        }

        return 'No se enviado Notificacion';

    }
}
