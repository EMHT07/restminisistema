<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Respuesta;
use App\Model\Cita;
use App\Model\Llamada;
use App\Model\Plan;
use App\Model\Cliente;

class LlamadaController extends Controller
{
    public function respuestas()
    {        
        $data = Respuesta::getRespuestas();

        return response()->json($data, 200);
    }

    // listado de citas
    public function cita(Request $request)
    {
        $data = Cita::getCitas($request);
        return response()->json($data, 200);
    }

    // Obtener el listado de clientes
    public function clienteLlamada(Request $request)
    {
        $data = Llamada::getClientes($request);     

        return response()->json($data[0], 200);
    }

    // Guarda la informacion del cliente
    public function registroLlamada(Request $request)
    {
        $data = Llamada::setRegistroLlamada($request);                

        $mensaje = explode(';', $data);

        return response()->json($mensaje[1], ($mensaje[0] === '0' ? 200: 400 ));
    }

    // Estadistica de los clientes
    public function estadistica(Request $request)
    {
        $data = Llamada::getEstadistica($request);

        $respuesta = explode(';', $data);

        $respArray = array(
            [
                'nombre' => 'Sin Concretar',
                'valor' => $respuesta[0]
            ],
            [ 
                'nombre' => 'Exitosa',
                'valor' => $respuesta[1]
            ],
            [ 
                'nombre'=> 'Fallidas', 
                'valor'=> $respuesta[2]
            ]
        );

        return response()->json($respArray, 200);
    } 
    
    // Modifica el id del cliente
    public function setLlamarCliente(Request $request)
    {
        $data = Llamada::setLlamarCliente($request);
        return response()->json($data, 200); 
    }

    // Mostrar planes de celuar
    public function getPlanes() {
        $data = Plan::getPlanes();
        return response()->json($data, 200);
    }
}
