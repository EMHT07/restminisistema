<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Usuario;

class UsuarioController extends Controller
{
    // login del usuario
    public function login(Request $request)
    {
        $data = Usuario::loginUsuario($request);        
        $codigo = isset($data) ? 200 : 401;
        
        return response()->json($data, $codigo);
    }

    // Informacion de Menus
    public function obtenerMenu(Request $request)
    {
        $data = Usuario::obtenerMenu($request);
        return response()->json($data, 200);
    }

    //
    public function estadisticaLlamadas(Request $request)
    {
        $data = Usuario::getEstadisticaLlamadas($request);

        return response()->json($data, 200);
    }
}
