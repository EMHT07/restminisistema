<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Empresa
Route::get('empresa', 'EmpresaController@informacion');

// Usuario
Route::post('login', 'UsuarioController@login');

// Obtener Menu
Route::get('obtenerMenu', 'UsuarioController@obtenerMenu');

// Llamadas
Route::get('cliente', 'LlamadaController@clienteLlamada');

Route::post('registro', 'LlamadaController@registroLlamada');

Route::get('estadistica', 'LlamadaController@estadistica');

Route::get('cita', 'LlamadaController@cita');

Route::post('llamarCliente', 'LlamadaController@setLlamarCliente');

Route::get('planCelular', 'LlamadaController@getPlanes');

// Parametros Sistema
Route::get('cronometro','ParametroController@cronometro');

// Tabla de maestros
Route::get('razon', 'UtilController@razon');
Route::get('respuesta', 'UtilController@respuesta');
Route::get('banco', 'UtilController@banco');
Route::get('formaPago', 'UtilController@formaPago');
Route::get('ventaTipo', 'UtilController@ventaTipo');

Route::get('datosIdentificacion', 'UtilController@getIdentificacion');

// Envio de correos
Route::get('envioCorreo', 'NotificacionController@envioCorreo');