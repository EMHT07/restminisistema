<?php

use Illuminate\Database\Seeder;

class MaestroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Model\EstadoCivil::class, 5)->create();
        factory(App\Model\Nacionalidad::class, 5)->create();
    }
}
