<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {     
        /*
        $this->call(EmpresaSeeder::class);     
        $this->call(UsuarioSeeder::class);                             
        
        $this->call(MaestroSeeder::class);       
                
        $this->call(ParroquiaSeeder::class);

        $this->call(CiudadSeeder::class);
        
        $this->call(SectorSeeder::class);
        */        
        
        $this->call(ClienteSeeder::class);
    }
}
