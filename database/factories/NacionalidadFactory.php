<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Nacionalidad::class, function (Faker $faker) {    
    return [
        'vchNacionalidad' => 'Ecuatoriano',       
        'intCodigoEstadoNacionalidad' => 1,
    ];
});
