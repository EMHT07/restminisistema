<?php

use Faker\Generator as Faker;

$factory->define(App\Model\EstadoCivil::class, function (Faker $faker) {    
    return [
        'vchEstadoCivil' => 'Soltero',
        'intCodigoEstadoEstadoCivil' => 1,
    ];
});
