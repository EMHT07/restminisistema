<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Usuario::class, function (Faker $faker) {
    return [
        'intCodigoEmpresa' => $faker->numberBetween($min = 1, $max = 5),
        'vchNombreUsuario' => $faker->firstname,
        'vchApellidoUsuario' => $faker->lastname,
        'vchDireccionUsuario' => $faker->address,
        'vchConvencionalUsuario' => $faker->numerify('02######') ,
        'vchCelularUsuario' => $faker->numerify('09########') ,
        'vchCorreoUsuario' => $faker->freeEmail,        
        'vchClaveUsuario' => '123456',     
        'vchImagenUsuario' => 'https://i.pravatar.cc/300',
        'vchCargoAsignado' => 'Sistemas',
        'intCodigoEstadoUsuario' => 1
    ];
});
