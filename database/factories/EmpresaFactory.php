<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Empresa::class, function (Faker $faker) {
    return [
        'vchNombreEmpresa' => $faker->firstname,
        'vchDireccionEmpresa' => $faker->address,
        'vchTelefonoEmpresa' => $faker->numerify('02########'),
        'vchLogoEmpresa' => 'assets/imagenes/'. $faker->numberBetween($min = 1, $max = 5).'.png',
        'vchCorreoEmpresa' => $faker->email ,
        'intCodigoEstadoEmpresa' => 1
    ];
});
