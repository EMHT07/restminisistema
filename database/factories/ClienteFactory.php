<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Cliente::class, function (Faker $faker) {
    return [
        'intCodigoSector' => 7,
        'intCodigoNacionalidad' => 1,
        'intCodigoEstadoCivil' => 1,
        'intCodigoIdentificacion' => 1,
        'vchIdentificacion' => $faker->unique()->regexify('[0-9]{9}'),
        'vchApellidoPaterno' => $faker->lastname,
        'vchApellidoMaterno' => $faker->lastname,
        'vchNombreCliente' => $faker->firstname,
        'vchConvencionalCliente' => $faker->numerify('02########'),
        'vchCelularCliente' => $faker->numerify('09########'),
        'vchCorreoCliente' => $faker->email,
        'vchDireccionCliente' => $faker->address,
        'vchNivelEducacion' => $faker->randomElement($array = array ('Bachiller','Ingeniero','Profesional')),
        'dtmFechaNacimiento' => $faker->randomElement($array = array ('1973-08-13','1983-10-23','1993-03-30', '1993-02-10')),        
        'dtmFechaRegistroCliente' => now()->format('Y-M-d'),
        'intCodigoEstadoCliente' => 1
    ];
});
