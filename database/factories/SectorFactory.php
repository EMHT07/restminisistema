<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Sector::class, function (Faker $faker) {
    return [
        'intCodigoParroquia' => $faker->numberBetween($min = 1, $max = 20),
        'vchNombreSector' => $faker->cityPrefix,
        'intCodigoEstadoSector' => 1,
    ];
});
