<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Menu::class, function (Faker $faker) {
    return [
        'intCodigoAsignacion' => $faker->numberBetween($min = 1, $max = 10),
        'vchNombreMenu' => $faker->randomElement($array = array ('Inicio','LLamadas','Configuración', 'Reportes')),
        'vchIconoMenu'=> $faker->randomElement($array = array ('fa fa-atlassian','fa fa-biking','fa fa-bitcoin', 'fa fa-brain')),
        'vchRutaMenu' => $faker->randomElement($array = array ('inicio','llamada','configuracion', 'reporte')),
        'intContieneSubMenu' => $faker->numberBetween($min = 0, $max = 1),
        'intEstadoMenu' => 1,   
    ];
});
