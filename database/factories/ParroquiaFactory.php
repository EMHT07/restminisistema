<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Parroquia::class, function (Faker $faker) {
    return [
        'intCodigoCiudad' => $faker->numberBetween($min = 1, $max = 20),
        'vchNombreParroquia' => $faker->state,
        'intCodigoEstadoParroquia' => 1,  
    ];
});
