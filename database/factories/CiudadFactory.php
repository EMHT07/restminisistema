<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Ciudad::class, function (Faker $faker) {    
    return [
        'intCodigoProvincia' => 1,
        'vchNombreCiudad' => $faker->city,
        'intCodigoEstadoCiudad' => 1,
    ];
});
