<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Permiso::class, function (Faker $faker) {
    return [
        'intCodigoUsuario' => $faker->numberBetween($min = 1, $max = 10),
        'vchNombrePermiso' => 'usuario',
        'dtmFechaCreacionPermiso' => now()->format('Y-M-d'),        
        'intCodigoEstadoPermiso' => 1
    ];
});
