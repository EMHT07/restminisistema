<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Asignacion::class, function (Faker $faker) {    
    return [
        'intCodigoUsuario' => $faker->numberBetween($min = 1, $max = 10),
        'vchNombreAsignacion' => $faker->randomElement($array = array ('usuario','vendedor','administrador')),
        'dtmFechaCreacion' => now()->format('Y-M-d h:m:s'),        
        'intEstadoAsignacion' => 1
    ];
});
